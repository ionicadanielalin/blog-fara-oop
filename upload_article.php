
<?php include
"includes/functions.php";
"includes/config.php";
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Blog Dan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="main.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-reboot.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<script>
    function changeLang() {
        document.getElementById('form_lang').submit();
    }
</script>
<?php include "parts/header.php";
include "parts/menu_admin.php";
?>


<div id="container">
    <h2 style="text-align: center">Introduceti articol</h2>
    <form method="post" action="add_article.php" enctype="multipart/form-data">
        <div class="mb-3">
            <label for="validationTextarea">Titlul articolului</label>
            <input name="title" type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
        </div>
        <div class="mb-3">
            <label for="validationTextarea">Titlul articolului in engleza</label>
            <input name="title_en" type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
        </div>

        <div class="mb-3">
            <label for="validationTextarea">Introduceti continutul articolului in romana</label>
            <input name="content" type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
        </div>
        <div class="mb-3">
            <label for="validationTextarea">Introduceti continutul articolului in engleza</label>
            <input name="content_en" type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
        </div>
        <div class="mb-3">
            <label for="validationTextarea">Introduceti numele autorului articolului</label>
            <input name="author" type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
        </div>
        <div class="form-group">
            <select name="category" class="custom-select" required>
                <option value="">Alegeti categoria articolului</option>
                <option value="Padurea">Padurea</option>
                <option value="Pomi seculari">Pomi seculari</option>
                <option value="Pomi traditionali">Pomi traditionali</option>
            </select>
        </div>

        <label for="validationTextarea">Introduceti o fotografie</label>
        <div style="color: crimson">Fotografia trebuie sa aiba o dimensiune de 259x194 px iar formatul trebuie sa fie
            .png
            Pentru a converti fotografia dumneavoastra, puteti folosi urmatorul link:
            <a href="https://image.online-convert.com/" class="button">Acceseaza</a>
        </div>

        <div class="mb-3">
            <input type="file" name="image">
        </div>


        <div class="row">
            <button type="submit" class="btn btn-primary mb-2">Incarcati articol</button>
            <div class="col-md-3"></div>
        </div>

    </form>
</div>


</body>
</html>


<?php include "parts/footer.php"; ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

</body>
</html>

