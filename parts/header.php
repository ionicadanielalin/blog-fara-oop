<?php
session_start();
?>

<div class="row">
<!-- Language -->
    <div class="col 4">
<form method='get' action='change_language.php' id='form_lang'>
    Alege o limba : <select name='lang' onchange='changeLang();'>
        <option value='ro' <?php if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'ro') {
            echo "selected";
        } ?> >Romana
        </option>
        <option value='en' <?php if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'en') {
            echo "selected";
        } ?> >English
        </option>
    </select>
</form>
    </div>
    <!-- Search buton -->
    <div class="col 4">
        <form action="search_last.php" method="get">
            <input type="text" name="search" placeholder="Searc for something..."/>
            <input type="submit"/>
        </form>
    </div>
    <!-- login -->
    <div class="col 4">

        <form action="login_cont.php" method="post">
            <div class="form-group">
                <input type="text" class="form-control" name="mail" placeholder="E-mail">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="Password">
            </div>
            <button type="submit" class="btn btn-primary">Intra in cont</button>
        </form>
    </div>
</div>


    <div id="header">
        <!-- Logo -->
        <div class="row">
            <div class="logo">
                <a href="#"><img src="images/logo.png" alt="logo"></a>
            </div>
            <div class="textLogo">
                <?php
                if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'en') {
                    echo "<h1>Call of nature: RICH COUNTRY</h1>";
                } else {
                    echo "<h1>CHEMAREA NATURII: BOGATIILE TARII</h1>";

                }
                ?>
            </div>
        </div>
        <!-- MENU -->
        <div id="menu">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="index.php">HOME</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <?php
                if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'en') {
                    $menuItems = dbSelect('menu', ['language' => 'en'], [], 0, null, 'language', 'ASC', null, 'OR');
                    //$menuItems = dbSelect('menu', null, null, 0, null, 'language', 'ASC', null);
                    //$menuItems = dbSelect('menu', ['language'=>'en'],null, 0, null, 'language', 'ASC', null);
                    //var_dump($menuItems);die;
                    ?>
                    <li class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <?php foreach ($menuItems as $menuItem): ?>
                                <li class="nav-item active"><a class="nav-link"
                                                               href="<?php echo $menuItem['url']; ?>"><?php echo $menuItem['title']; ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                    <?php
                } else {
                    $menuItems = dbSelect('menu', ['language' => 'ro'], [], 0, null, 'language', 'ASC', null);
                    //$menuItems = dbSelect('menu', ['language'=>'ro'],null, 0, null, 'language', 'ASC', null);
                    ?>
                    <li class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <?php foreach ($menuItems as $menuItem): ?>
                                <li class="nav-item active"><a class="nav-link"
                                                               href="<?php echo $menuItem['url']; ?>"><?php echo $menuItem['title']; ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                    <?php
                }
                ?>
        </div>
    </div>
</div>


