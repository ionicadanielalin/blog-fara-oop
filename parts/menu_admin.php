
<!-- User -->
<div>
    <h3 style="text-align: center">USER: <?php echo $_SESSION['username']; ?></h3>
</div>


<!-- MENU -->
<div id="menu">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <?php
        if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'en') {
            $menuItems = dbSelect('menu_admin', ['language' => 'en'], [], 0, null, 'language', 'ASC', null, 'OR');
            //$menuItems = dbSelect('menu', null, null, 0, null, 'language', 'ASC', null);
            //$menuItems = dbSelect('menu', ['language'=>'en'],null, 0, null, 'language', 'ASC', null);
            //var_dump($menuItems);die;
            ?>
            <li class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <?php foreach ($menuItems as $menuItem): ?>
                        <li class="nav-item active"><a class="nav-link"
                                                       href="<?php echo $menuItem['url']; ?>"><?php echo $menuItem['title']; ?></a>
                        </li>
                    <?php endforeach; ?>

                </ul>
            </li>
            <?php
        } else {
            $menuItems = dbSelect('menu_admin', ['language' => 'ro'], [], 0, null, 'language', 'ASC', null);
            //$menuItems = dbSelect('menu', ['language'=>'ro'],null, 0, null, 'language', 'ASC', null);
            ?>
            <li class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <?php foreach ($menuItems as $menuItem): ?>
                        <li class="nav-item active"><a class="nav-link"
                                                       href="<?php echo $menuItem['url']; ?>"><?php echo $menuItem['title']; ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </li>
            <?php
        }
        ?>
    </nav>
</div>
