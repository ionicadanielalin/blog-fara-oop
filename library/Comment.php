<?php

/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 8/20/2019
 * Time: 5:33 PM
 */
class Comment extends BaseEntity
{

    public $nickname;
    public $email;
    public $content;
    public $status;
    public $product_id;
    public $date;
    public $time;


    public function getTable()
    {
        return "comments";
    }
}