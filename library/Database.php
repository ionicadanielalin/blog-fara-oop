<?php

/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 8/20/2019
 * Time: 5:34 PM
 */
class Database
{
    private $hostdb = "77.81.105.198";
    private $userdb = "root";
    private $passdb = "scoalait123";
    private $namedb = "ionica_blog";
    public $pdo;

    public function __construct()
    {
        if (!isset($this->pdo)) {
            try {
                $link = new PDO("mysql:host=" . $this->hostdb . ";dbname=" . $this->namedb, $this->userdb, $this->passdb);
                $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $link->exec('SET CHARACTER SET utf8');
                $this->pdo = $link;

            } catch (PDOException $e) {
                die("Failed to connect with Database" . $e->getMessage());
            }
        }
    }
}