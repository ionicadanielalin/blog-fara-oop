<?php

/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 8/20/2019
 * Time: 5:32 PM
 */
class Category extends BaseEntity
{
    public $id;

    public $name;

    public $parent_id;

    public function getProducts()
    {
        $data = dbSelect('product',['category_id'=>$this->id]);

        $result = [];
        foreach ($data as $productData){
            $result[]=new Article($productData['id']);
        }

        return $result;
    }
}