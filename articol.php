<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog Dan</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="main.css"/>
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-reboot.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<?php
include "includes/functions.php";
include "parts/header.php";
?>

<div id="container">
    <div class="article">
        <?php
        $articles = dbSelect('articles', ['id' => $_GET['key']], [], 0, null, 'category', 'ASC', 'category');
        $article = $articles[0];
        showAllArticle($article);
        ?>
    </div>
    <br>
</div>



<div>
    <h2>ARTICOLE SIMILARE</h2>
</div>

<div id="container">
    <div class="row">
        <?php
        $articles = dbSelect('articles', null, [], 0, null, 'RAND()', 'ASC', null);
        //$result = mysqli_query($mysqlConnect, "SELECT * FROM articles ORDER BY RAND ()");
        //$articles = $result->fetch_all(MYSQLI_ASSOC);
        foreach ($articles as $key => $article) {
            showSimilarArticle($article);
        }
        ?>
    </div>
</div>

<div class="article-comments">
    <?php
    //$result = mysqli_query($mysqlConnect, "SELECT * FROM comments  WHERE status ='visible' AND article_id=".$_GET['key']);
    //global $mysqlConnect;
    $comments = dbSelect('comments', ['status' => 'visible','article_id'=>$_GET['key']], [], 0, null, null, 'ASC', null, 'AND');
    //var_dump($comments);
    //['id' => $_GET['key'],'status'=>'visible']
    //$comments = $result->fetch_all(MYSQLI_ASSOC);
    foreach ($comments as $comment): ?>
        <div class="alert alert-success" role="alert">
            <a href="#" class="badge badge-warning"><?php echo $comment['nickname'] ?></a>
            <?php echo $comment['content'] ?>
        </div>
    <?php endforeach;?>
</div>
<hr />


<div class="article-form">
    <p>Va rugam sa folositi un limbaj civilizat!!</p>

    <form action="add-comment.php?key=<?php echo $_GET['key']; ?>" method="post">

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">Nickname</span>
            </div>
            <input name="nickname" type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
        </div>


        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">Comentariul tau</span>
            </div>
            <textarea name="comment" class="form-control" aria-label="With textarea"></textarea>
        </div>
        <button type="submit" class="btn btn-primary mb-2">Adauga comentariu</button>
    </form>
</div>







<?php include 'parts/footer.php'; ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>
