<?php
include "includes/functions.php";
include "includes/config.php";
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Blog Dan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="main.css"/>
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-reboot.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<script>
    function changeLang() {
        document.getElementById('form_lang').submit();
    }
</script>

<?php
include "parts/header.php";
?>


<div id="container">
    <div class="row">

        <div class="contentIndex">
            <div>
                <?php
                $articles = dbSelect('articles', null, null, 0, null, null, null, null, "AND");
                foreach ($articles as $key => $article) {
                    if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'en') {
                        $article['titlex'] = $article['title_en'];
                        $article['contentx'] = $article['content_en'];
                        showArticle($article);
                    } else {
                        $article['titlex'] = $article['title'];
                        $article['contentx'] = $article['content'];
                        showArticle($article);
                    }
                }
                ?>
            </div>
        </div>

        <div>
            <?php include "parts/sidebar.php" ?>
        </div>
    </div>
</div>

<?php include "parts/footer.php"; ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>

