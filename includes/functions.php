<?php
include "config.php";

function dbDelete($table, $id)
{
    global $mysqlConnect;
    $result = mysqli_query($mysqlConnect, "DELETE FROM $table WHERE id=" . intval($id));

    return mysqli_affected_rows($mysqlConnect) > 0;
}

function dbUpdate($table, $id, $data)
{
    global $mysqlConnect;
    $sets = [];
    foreach ($data as $column => $value) {
        //$sets[]=" `$column`='$value'";
        //var_dum($sets);die;
        $sets[] = "`" . mysqli_real_escape_string($mysqlConnect, $column) . "`='" . mysqli_real_escape_string($mysqlConnect, $value) . "'";
    }
    $sqlSets = implode(',', $sets);
    $result = mysqli_query($mysqlConnect, "UPDATE $table SET $sqlSets  WHERE id=" . intval($id));
    return mysqli_affected_rows($mysqlConnect) > 0;

}


function dbSelect($table, $filters = null, $likeFilters = null, $offset = 0, $limit = null, $sortBy = null, $sortDirection = 'ASC', $groupBy = null, $operator = 'AND')
{
    global $mysqlConnect;
    $sql = "SELECT * FROM $table";

    if (($filters != null) || ($likeFilters != null)) {
        $sets = [];
        if ($filters != null) {
            foreach ($filters as $column => $value) {
                if ($value != null) {
                    $sets[] = "`" . mysqli_real_escape_string($mysqlConnect, $column) . "`='" . mysqli_real_escape_string($mysqlConnect, $value) . "'";
                }
            }
        }
        if ($likeFilters != null) {
            foreach ($likeFilters as $column => $value) {
                if ($value != null) {
                    $sets[] = "`" . mysqli_real_escape_string($mysqlConnect, $column) . "` LIKE '%" . mysqli_real_escape_string($mysqlConnect, $value) . "%'";


                }

            }
        }
        $sql .= ' WHERE ' . implode($operator, $sets);
    }

    if ($groupBy) {
        $sql .= ' GROUP BY ' . mysqli_real_escape_string($mysqlConnect, $groupBy);

    }
    if ($sortBy != null) {
        $sql .= ' ORDER BY ' . mysqli_real_escape_string($mysqlConnect, $sortBy) . ' ' . mysqli_real_escape_string($mysqlConnect, $sortDirection);
    }

    if ($limit != null) {
        $sql .= ' LIMIT ' . intval($offset) . ',' . intval($limit);
    }
    //die($sql);
    $result = mysqli_query($mysqlConnect, $sql);
    if (!$result) {
        die("SQL error: " . mysqli_error($mysqlConnect) . " SQL:" . $sql);
    }

    return $result->fetch_all(MYSQLI_ASSOC);

}


function dbSelectOne($table, $filters = [], $likeFilters = [], $offset = 0, $limit = null, $sortBy = null, $sortDirection = 'ASC')
{
    $data = dbSelect($table, $filters, $likeFilters, 0, 1, $sortBy, $sortDirection);
    return $data[0];
}


function dbInsert($table, $data)
{
    global $dbConnection;
    $columns = [];
    $values = [];
    $images = $_FILES['image'];

    foreach ($data as $column => $value) {
        $columns[] = "`" . mysqli_real_escape_string($dbConnection, $column) . "`";
        $values[] = "'" . mysqli_real_escape_string($dbConnection, $value) . "'";
    }
    $sqlcolumns = implode(',', $columns);
    $sqlvalues = implode(',', $values);
    $result = mysqli_query($dbConnection, "INSERT INTO $table($sqlcolumns) VALUES ($sqlvalues)");
    return mysqli_insert_id($dbConnection);
}


function showArticle($article)
{
    ?>
    <div>

        <h2><a href="articol.php?key=<?php echo $article['id']; ?>"><?php echo $article['titlex']; ?></a></h2>
        <a href="articol.php?key=<?php echo $article['id']; ?>"><img align="center"
                                                                     src="images/<?php echo $article['image']; ?>"
                                                                     width="60%"></a>
        <p>
            <?php echo substr($article['contentx'], 0, strpos($article['contentx'], ' ', 255)); ?>...
        </p>
        <p>By: <i><?php echo $article['author']; ?></i></p>
        <br>
    </div>
    <?php
}



function show_log_Article($article)
{
    ?>
    <div>

        <h2><a href="articol.php?key=<?php echo $article['id']; ?>"><?php echo $article['title']; ?></a></h2>
        <a href="articol.php?key=<?php echo $article['id']; ?>"><img align="center"
                                                                     src="images/<?php echo $article['image']; ?>"
                                                                     width="60%"></a>
        <p>
            <?php echo($article['content']); ?>
        </p>
        <p>By: <i><?php echo $article['author']; ?></i></p>
        <br>
            <div>
                <a class="btn btn-danger" href="admin_article.php?key=<?php echo $article['id']; ?>" role="button">Editeaza</a>
            </div>
    </div>
    <?php
}


function showSimilarArticle($article)
{
    ?>
    <div class="col-4">
        <h2><a href="articol.php?key=<?php echo $article['id']; ?>"><?php echo $article['title']; ?></a></h2>
        <a href="articol.php?key=<?php echo $article['id']; ?>"><img align="center"
                                                                     src="images/<?php echo $article['image']; ?>"
                                                                     width="60%"></a>
        <p>
            <?php echo substr($article['content'], 0, strpos($article['content'], ' ', 255)); ?>...
        </p>
        <p>By: <i><?php echo $article['author']; ?></i></p>
        <br>
    </div>
    <?php
}


function showAllArticle($article)
{
    ?>
    <div>
        <h2 style="text-align: center"><a
                    href="articol.php?key=<?php echo $article['id']; ?>"><?php echo $article['title']; ?></a></h2>
        <a href="articol.php?key=<?php echo $article['id']; ?>"><img align="center"
                                                                     src="images/<?php echo $article['image']; ?>"
                                                                     width="80%"></a>
        <p>
            <?php echo $article['content']; ?>
        </p>
        <p style="text-align: center">By: <i><?php echo $article['author']; ?></i></p>
        <br>
    </div>
    <?php
}






