<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog Dan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="main.css"/>
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-reboot.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<?php
include "includes/functions.php";
include "parts/header.php";
include "parts/menu_admin.php";
?>

<div id="container">
    <div class="article">
        <?php
        $articles = dbSelect('articles', ['id' => $_GET['key']], [], 0, null, 'category', 'ASC', 'category');
        $article = $articles[0];
        show_log_Article($article);
        ?>
    </div>
    <br>
</div>



<div id="container">
    <h2 style="text-align: center">Editeaza articol</h2>
    <form method="post" action="edit_article_header.php?key=<?php echo $_GET['key']; ?>" enctype="multipart/form-data">
        <div class="mb-3">
            <label for="validationTextarea">id articol</label>
            <input name="id" type="text" class="form-control" aria-label="Amount (to the nearest dollar)"
                   value="<?php echo $articles[0]['id'] ?>">
        </div>



        <div class="mb-3">
            <label for="validationTextarea">Titlul articolului in romana</label>
            <input name="title" type="text" class="form-control" aria-label="Amount (to the nearest dollar)"
                   value="<?php echo $articles[0]['title'] ?>">
        </div>
        <div class="mb-3">
            <label for="validationTextarea">Titlul articolului in engleza</label>
            <input name="title_en" type="text" class="form-control" aria-label="Amount (to the nearest dollar)"
                   value="<?php echo $articles[0]['title_en'] ?>">
        </div>

        <div class="mb-3">
            <label for="validationTextarea"> Continutul articolului in romana</label>
            <textarea name="content" datatype="longtext" class="form-control"
                      id="formGroupExampleInput2"><?php echo $articles[0]['content'] ?></textarea>
        </div>
        <div class="mb-3">
            <label for="validationTextarea">Continutul articolului in engleza</label>
            <textarea name="content_en" datatype="longtext" class="form-control"
                      id="formGroupExampleInput2"><?php echo $articles[0]['content_en'] ?></textarea>
        </div>
        <div class="mb-3">
            <label for="validationTextarea"> Numele autorului articolului</label>
            <input name="author" type="text" class="form-control" aria-label="Amount (to the nearest dollar)"
                   value="<?php echo $articles[0]['author'] ?>">
        </div>
        <div class="mb-3">
            <button class="btn btn-outline-secondary" type="button">Limba</button>
        </div>
        <div class="form-group">
            <select name="language" class="custom-select" required>
                <option value="<?php echo $articles[0]['language'] ?>"
                        selected><?php echo $articles[0]['language'] ?></option>
                <option value="ro">Romana</option>
                <option value="en">Engleza</option>
            </select>
        </div>

        <div class="form-group">
            <select name="category" class="custom-select" required>
                <option value="<?php echo $articles[0]['category'] ?>"
                        selected><?php echo $articles[0]['category'] ?></option>
                <option value="">Alegeti categoria articolului</option>
                <option value="Padurea">Padurea</option>
                <option value="Pomi seculari">Pomi seculari</option>
                <option value="Pomi traditionali">Pomi traditionali</option>
            </select>
        </div>
        <div style="color: crimson">Fotografia trebuie sa aiba o dimensiune de 259x194 px iar formatul trebuie sa fie
            .png
            Pentru a converti fotografia dumneavoastra, puteti folosi urmatorul link:
            <a href="https://image.online-convert.com/" class="button">Acceseaza</a>
        </div>

        <div class="mb-3">
            <b><label for="formGroupExampleInput2">Incarca o imagine</label></b>
            <input type="file" name="image" value="<?php echo $articles[0]['image'] ?>">
        </div>
        <div class="row">
            <div class="col-md-6">
            <button type="submit" class="btn btn-primary mb-2">Salveaza</button>
            </div>
            <div class="col-md-6">
            <a class="btn btn-danger" href="delete_article.php?key=<?php echo $_GET['key']; ?>" role="button">Sterge articol</a>
            </div>
        </div>

    </form>
</div>

<div>
    <h2>ARTICOLE SIMILARE</h2>
</div>

<div id="container">
    <div class="row">
        <?php
        $articles = dbSelect('articles', null, [], 0, null, 'RAND()', 'ASC', null);
        //$result = mysqli_query($mysqlConnect, "SELECT * FROM articles ORDER BY RAND ()");
        //$articles = $result->fetch_all(MYSQLI_ASSOC);
        foreach ($articles as $key => $article) {
            showSimilarArticle($article);
        }
        ?>
    </div>
</div>

<div class="article-comments">
    <?php
    //$result = mysqli_query($mysqlConnect, "SELECT * FROM comments  WHERE status ='visible' AND article_id=".$_GET['key']);
    //global $mysqlConnect;
    $comments = dbSelect('comments', ['status' => 'visible','article_id'=>$_GET['key']], [], 0, null, null, 'ASC', null, 'AND');
    //var_dump($comments);
    //['id' => $_GET['key'],'status'=>'visible']
    //$comments = $result->fetch_all(MYSQLI_ASSOC);
    foreach ($comments as $comment): ?>
        <div class="alert alert-success" role="alert">
            <a href="#" class="badge badge-warning"><?php echo $comment['nickname'] ?></a>
            <?php echo $comment['content'] ?>
        </div>
    <?php endforeach;?>
</div>
<hr />


<div class="article-form">
    <p>Va rugam sa folositi un limbaj civilizat!!</p>

    <form action="add-comment.php?key=<?php echo $_GET['key']; ?>" method="post">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">Nickname</span>
            </div>
            <input name="nickname" type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
        </div>


        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">Comentariul tau</span>
            </div>
            <textarea name="comment" class="form-control" aria-label="With textarea"></textarea>
        </div>
        <button type="submit" class="btn btn-primary mb-2">Adauga comentariu</button>
    </form>
</div>







<?php include 'parts/footer.php'; ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>
